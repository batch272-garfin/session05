
-- 1.

SELECT customerName FROM customers WHERE country = "Philippines";

-- 2.

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3.

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- 4.

SELECT lastName, firstName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5.

SELECT customerName FROM customers WHERE state IS NULL;

-- 6.

SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- 7.

SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 8.

SELECT orderdetails.quantityOrdered
FROM orderdetails
JOIN orders ON orderdetails.orderNumber = orders.orderNumber
WHERE orders.comments LIKE "%DHL%";

-- 9.

SELECT productLine from productlines WHERE textDescription LIKE "%state of the art%";

-- 10.

SELECT DISTINCT country FROM customers;

-- 11.

SELECT DISTINCT status FROM orders;

-- 12.

SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

-- 13.

SELECT firstName, lastName, offices.city
FROM employees
JOIN offices ON employees.officeCode = offices.officeCode
WHERE offices.city = "Tokyo";

-- 14.

SELECT customerName
FROM customers
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";

-- 15.

SELECT products.productName, customers.customerName
FROM customers
JOIN orders ON customers.customerNumber = orders.customerNumber
JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
JOIN products ON orderdetails.productCode = products.productCode
WHERE customers.customerName = "Baane Mini Imports";

-- 16.

SELECT employees.firstName, employees.lastName, customers.customerName, offices.country
FROM customers
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
JOIN offices ON employees.officeCode = offices.officeCode
WHERE customers.country = offices.country;

-- 17.

SELECT productName, quantityInStock
FROM products
WHERE productLine = "Planes"
AND quantityInStock < 1000;

-- 18.

SELECT customerName FROM customers
WHERE phone LIKE "%+81%";